/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));
//
// const app = new Vue({
//     el: '#app'
// });

$(document).ready(function() {

    // Add to cart
    $('.add-to-cart').click(function(e) {
        e.preventDefault();
        var dish_id = $(this).data('id');
        // $('#cart-menu').removeClass("hidden");
        $.ajax({
            url: window.Laravel.cartAddRoute,
            method: 'post',
            data: {
                _token: window.Laravel.csrfToken,
                id: dish_id
            },
            success: function(response) {
                $('#grand_total').html(response.total);
                refreshCart(response.items);
            }
        })
    })


    // Clear cart
    $('.clear-cart').click(function(e) {
        e.preventDefault();
      // $('#cart-menu').addClass("hidden");
        $.ajax({
            url: window.Laravel.cartClearRoute,
            method: 'delete',
            data: {
                _token: window.Laravel.csrfToken,
            },
            success: function(response) {
                $('#grand_total').html(0);
                refreshCart([]);
            }
        });
    });


function refreshCart(items){
    var list = $('#cart-items');
    list.empty();

    for (var i = 0; i < items.length; i++) {
        var item =
            '<li>' +
            '<a href="#">' + items[i].title + ' x ' + items[i].quantity + ' = ' + items[i].total + ' &euro; </a></li>';
        list.append(item);
    }
}
})
