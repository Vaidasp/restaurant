@extends('layouts.app')

@section('content')
  <div class="container">
    @if (isset($order))
      <h1>Order edit</h1>
    @else
      <h1>Order create</h1>
    @endif
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class="row">
      <div class="col-md-3">
            @if (isset($order))
              {!! Form::model($order, ['route' => ['orders.update', $order->id],'method' => 'put']) !!}
            @else
              {!! Form::open([
                'route' => 'orders.store',
                'method' =>'post'
                ]) !!}
              @endif
              <div class="form-group {{ $errors->has('client_name') ? ' has-error' : '' }}">
                <label for="client_name">Client name:</label>
                {!!Form::text('client_name', $order->client_name, ['class'=>"form-control",'placeholder'=>'Name'])!!}
              </div>

              <div class="form-group">
                <label for="client_email">Client email:</label>
                {!!Form::email('client_email',$order->client_email,['class'=>"form-control",'placeholder'=>'Email'])!!}
              </div>
              <div class="form-group">
                <label for="client_phone">Client phone:</label>
                {!!Form::text('client_phone',Auth::user() ? Auth::user()->phone : null,['class'=>"form-control",'placeholder'=>'Email'])!!}
              </div>
              <div class="form-group">
                <label for="persons">How many persons?:</label>
                {!!Form::selectRange('persons', 1, 8);!!}
              </div>
              <div class="form-group">
                <label for="table">Please select table:</label>
                {!!Form::select('table',$tables,null,['class'=>"form-control"])!!}
              </div>
              <div class="form-group">
                <label for="table">Please select date:</label>
                {!!Form::date('reservation_date',null,['class'=>"form-control"])!!}
              </div>
              <div class="form-group">
                <label for="time">Please select time:</label>
                {!!Form::time('reservation_time',null,['class'=>"form-control"])!!}
              </div>

              @if (session('cart.total'))
                <p>Total: {{session('cart.total')}} &euro;</p>
              @endif
              <div class="btn-group">
                @if (isset($order))
                  <a class="btn btn-default" href="{{ route('orders.index') }}">Back to orders</a>
                  {!!Form::submit('Save',['class' => 'btn btn-primary'])!!}
                  {!! Form::close() !!}
                @else
                  {!!Form::submit('Place order',['class' => 'btn btn-primary'])!!}
                  {!! Form::close() !!}
                @endif
              </div>
        </div>
          <div class="col-md-9">
            <table class="table table-bordered table-responsive checkout-table">
              <thead>
                <tr>
                  <th>
                    Actions
                  </th>
                  <th>
                    Photo
                  </th>
                  <th>
                    Title
                  </th>
                  <th>
                    Price
                  </th>
                  <th>
                    Netto price
                  </th>
                  <th>
                    Quantity
                  </th>
                  <th>
                    Total
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($order->order_lines as $item)
                <tr>
                  <td class="">
                      @if (Auth::check() && Auth::user()->isAdmin())

                        <div class="btn-group">
                          {!! Form::open([
                            'route' => ['orders.destroyLine', $item->id],
                            'method' =>'delete',
                            'class'=>'btn-group'
                            ]) !!}
                            {!!Form::submit('X',['class' => 'btn btn-sm btn-danger'])!!}

                            {!! Form::close() !!}
                          </div>
                      @else
                        <div class="btn-group">
                          <a class="btn btn-sm btn-default" href="{{ route('orders.show', ['id' => $order->id]) }}">Show</a>
                          </div>
                      @endif

                  </td>
                  <td>
                    <img class="img-responsive" src="{{$item->dish->photo}}" alt="{{$item->dish->title}}">
                  </td>
                  <td>
                    {{$item->dish->title}}
                  </td>
                  <td>
                    {{number_format($item->dish->price,2)}} &euro;
                  </td>
                  <td>
                    {{number_format($item->dish->netto_price,2)}} &euro;
                  </td>
                  <td>
                    {{$item->quantity}}
                  </td>
                  <td>
                    {{number_format($item['total'],2)}} &euro;
                  </td>
                </tr>
                @endforeach
              </tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                  Total:
                </td>
                <td>
                  {{$order->total}} &euro;
                </td>
              </tr>
              </tbody>
            </table>
              <div>
                {!! Form::open([
                  'route' => ['orders.addLine', $order->id],
                  'method' =>'post',
                  'class'=>'form-inline',
                  ]) !!}
                <div class="form-group">
                  <label for="dish_id">Select dish:</label>
                  {!!Form::select('dish_id',$dishes,null,['class'=>"form-control"])!!}
                </div>
                <div class="form-group">
                  <label for="dish_quantity">Select quantity:</label>
                  {!!Form::selectRange('dish_quantity', 1, 10);!!}
                </div>
                {!!Form::submit('Add',['class' => 'btn btn-primary'])!!}
                {!! Form::close() !!}
              </div>
          </div>
      </div>
    </div>
  @endsection
