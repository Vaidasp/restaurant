@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <h1>Orders list</h1>
        <table class="table table-bordered">
	<thead>
		<tr>
			<th>
				Actions
			</th>
      <th>
        Order Id
      </th>
			<th>
				Name
			</th>
			<th>
				Email
			</th>
			<th>
				Total
			</th>
			<th>
				 Reservation date
			</th>
		</tr>
	</thead>
	<tbody>
    @foreach ($orders as $order)
      @if (Auth::check() && Auth::user()->isAdmin())
      <tr class="{{$order->getOrderStatus()}}">
      @else
        <tr>
      @endif
        <td class="table-edit">
            @if (Auth::check() && Auth::user()->isAdmin())

              <div class="btn-group">
                <a class="btn btn-sm btn-default" href="{{ route('orders.show', ['id' => $order->id]) }}">Show</a>
                <a class="btn btn-sm btn-success" href="{{ route('orders.edit', ['id' => $order->id]) }}">Edit</a>
                {!! Form::open([
                  'route' => ['orders.destroy', $order->id],
                  'method' =>'delete',
                  'id'=> 'order-id'.$order->id,
                  'class'=>'btn-group'
                  ]) !!}
                  {!!Form::submit('X',['class' => 'btn btn-sm btn-danger'])!!}

                  {!! Form::close() !!}
                </div>
            @else
              <div class="btn-group">
                <a class="btn btn-sm btn-default" href="{{ route('orders.show', ['id' => $order->id]) }}">Show</a>
                </div>
            @endif

        </td>
        <td>
          <a class="" href="{{ route('orders.show', ['id' => $order->id]) }}">#{{ $order->id }}</a>
        <td>
          {{ $order->client_name }}
        </td>
        <td>
          {{ $order->client_email }}
        </td>
        <td>
          {{ $order->total }}
        </td>
        <td>
          {{ $order->reservation_date }}
        </td>
      </tr>
    @endforeach
	</tbody>
</table>
      </div>
    </div>
  </div>
@endsection
