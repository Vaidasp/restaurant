@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <h1>Order number: {{$order->id}} </h1>
        <h2>Client: {{$order->client_name}} </h2>
        <h2>Table: {{$order->table->title}} </h2>
        <h2>Number of persons: {{$order->number_of_persons}} </h2>
        <h2>Reservation time: {{$order->reservation_date}}  {{$order->reservation_time}}</h2>
        <h1>Ordered dishes:</h1>
            <table class="table table-bordered table-responsive checkout-table">
              <thead>
                <tr>
                  <th>
                    Photo
                  </th>
                  <th>
                    Title
                  </th>
                  <th>
                    Price
                  </th>
                  <th>
                    Netto price
                  </th>
                  <th>
                    Quantity
                  </th>
                  <th>
                    Total
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($order->order_lines as $item)
                <tr>
                  <td>
                    <img class="img-responsive" src="{{$item->dish->photo}}" alt="{{$item->dish->title}}">
                  </td>
                  <td>
                    {{$item->dish->title}}
                  </td>
                  <td>
                    {{number_format($item->dish->price,2)}} &euro;
                  </td>
                  <td>
                    {{number_format($item->dish->netto_price,2)}} &euro;
                  </td>
                  <td>
                    {{$item->quantity}}
                  </td>
                  <td>
                    {{number_format($item['total'],2)}} &euro;
                  </td>
                </tr>
                @endforeach
              </tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                  Total:
                </td>
                <td>
                  {{$order->total}} &euro;
                </td>
              </tr>
              </tbody>
            </table>
      </div>
    </div>
  </div>
@endsection
