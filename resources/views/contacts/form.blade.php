@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        @if (isset($contact))
          <h1>Contacts edit</h1>
        @else
          <h1>Contacts create</h1>
        @endif
        <div class="row">
            @if (isset($contact))
              {!! Form::model($contact, ['route' => ['contacts.update', $contact->id],'method' => 'put']) !!}
            @else
              {!! Form::open([
                'route' => 'contacts.store',
                'method' =>'post'
                ]) !!}
              @endif
              <div class="form-group">
                <label for="title">Restaurant title:</label>
                {!!Form::text('title',null,['class'=>"form-control",'placeholder'=>'Title'])!!}
              </div>
              <div class="form-group">
                <label for="address">Restaurant address:</label>
                {!!Form::text('address',null,['class'=>"form-control",'placeholder'=>'Address'])!!}
              </div>

              <div class="form-group">
                <label for="email">Restaurant email:</label>
                {!!Form::text('email',null,['class'=>"form-control",'placeholder'=>'Email'])!!}
              </div>

              <div class="form-group">
                <label for="phone">Restaurant phone:</label>
                {!!Form::text('phone',null,['class'=>"form-control",'placeholder'=>'Phone'])!!}
              </div>

              <div class="form-group">
                <label for="hours">Restaurant working hours:</label>
                {!!Form::textarea('hours',null,['class'=>"form-control",'placeholder'=>'Hours', 'id'=>'hours'])!!}
              </div>

              <div class="form-group">
                <label for="map">Restaurant map:</label>
                {!!Form::text('map',null,['class'=>"form-control",'placeholder'=>'Map URL'])!!}
              </div>

              <div class="btn-group">
                @if (isset($contact))
                  {!!Form::submit('Save',['class' => 'btn btn-primary'])!!}
                  {!! Form::close() !!}
                @else
                  {!!Form::submit('Submit',['class' => 'btn btn-primary'])!!}
                  {!! Form::close() !!}
                @endif
              </div>
          </div>
        </div>
      </div>
    </div>
  @endsection
