@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <h1>{{$contact->title}}</h1>
        @if (Auth::check() && Auth::user()->isAdmin())
          <a class="" href="{{ url('contacts/1/edit') }}">
            <p>+ Edit contacts</p>
            </a>
          @endif
        <div class="row">
            <div class="col-md-6">
              <h3>Address: {{$contact->address}}</h3>
              <h3>Phone: {{$contact->phone}}</h3>
              <h3>Email: <a href= "mailto:{{$contact->email}}">{{$contact->email}}</a></h3>
            </div>
            <div class="col-md-6">
              <h3>Working hours:</h3>
              {!!$contact->hours!!}
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-md-12">
              <iframe src="{{$contact->getMapSrc()}}" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection
