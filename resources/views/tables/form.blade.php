@extends('layouts.app')

@section('content')
  <div class="container">
    @if (isset($dish))
      <h1>Table edit</h1>
    @else
      <h1>Table create</h1>
    @endif
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          @if (isset($table))
            <div class="col-md-4">
              <img style="margin: 0 auto;" class="img-responsive" src="{{$table->photo}}?text={{$table->title}}" alt="{{$table->title}}">
            </div>
          @endif
          <div class="col-md-8">
            @if (isset($table))
              {!! Form::model($table, ['route' => ['tables.update', $table->id],'method' => 'put']) !!}
            @else
              {!! Form::open([
                'route' => 'tables.store',
                'method' =>'post'
                ]) !!}
              @endif
              <div class="form-group">
                <label for="title">Table title:</label>
                {!!Form::text('title',null,['class'=>"form-control",'placeholder'=>'Title'])!!}

                <label for="netto_price">Min persons:</label>
                {!!Form::number('min',null,['class'=>"form-control",])!!}

                <label for="max">Max persons:</label>
                {!!Form::number('max',null,['class'=>"form-control",])!!}
              </div>
              <div class="btn-group">
                @if (isset($table))
                  <a class="btn btn-default" href="{{ route('tables.index') }}">Back to tables</a>
                  {!!Form::submit('Save',['class' => 'btn btn-primary'])!!}
                  {!! Form::close() !!}
                @else
                  {!!Form::submit('Submit',['class' => 'btn btn-primary'])!!}
                  {!! Form::close() !!}
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endsection
