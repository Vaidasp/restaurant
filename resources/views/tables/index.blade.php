@extends('layouts.app')

@section('content')
  <div class="container">
      <div class="btn-group">
      </div>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        @if (Auth::check() && Auth::user()->isAdmin())
          <a class="" href="{{ route('tables.create') }}">
            <p>+ Create table</p>
            </a>
          @endif
        @foreach ($tables as $table)
          <div class="row table">
            <div class="col-md-4">
              <img class="img-responsive" src="{{$table->photo}}?text={{$table->title}}" alt="{{$table->title}}">
            </div>
            <div class="col-md-8">
              <h1>Table: {{$table->title}}</h1>
              <h2>Min persons: {{$table->min}}</h2>
              <h2>Max persons: {{$table->max}}</h2>

            </div>
          </div><br>
        @endforeach
      </div>
    </div>
  </div>
@endsection
