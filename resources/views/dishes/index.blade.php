@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        @if (Auth::check() && Auth::user()->isAdmin())
          <a class="" href="{{ route('dishes.create') }}">
            <p>+ Create dish</p>
            </a>
          @endif
        @foreach ($dishes as $dish)
          <div class="row dish">
            <div class="col-md-4">
              <a  href="{{ route('dishes.show', ['id' => $dish->id]) }}"><img class="img-responsive" src="{{$dish->photo}}?text={{$dish->title}}" alt="{{$dish->title}}"></a>
            </div>
            <div class="col-md-8">
              <h1>{{$dish->title}}</h1>
              <p>{{$dish->description}}</p>
              <p><strong>Kaina: </strong>{{ number_format($dish->price,2) }} &euro;</p>
              @if (Auth::check())
                <a  class="btn btn-success btn-sm add-to-cart" data-id="{{$dish->id}}"  href="#"><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</a>
              @endif
            </div>
          </div><br>
        @endforeach
      </div>
    </div>
  </div>
@endsection
