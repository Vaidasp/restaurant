@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-4">
            <img style="margin: 0 auto;" class="img-responsive" src="{{$dish->photo}}?text={{$dish->title}}" alt="{{$dish->title}}">
          </div>
          <div class="col-md-8">
            <h1>{{$dish->title}}</h1>
            <p>{{$dish->description}}</p>
            <p>Price: {{ number_format($dish->price,2) }} &euro;</p>
            <div class="btn-group">
              <a class="btn btn-primary" href="{{ route('dishes.index') }}">Back to dishes</a>
              @if (Auth::check() && Auth::user()->isAdmin())
                <a class="btn btn-success" href="{{ route('dishes.edit', ['id' => $dish->id]) }}">Edit</a>
                {!! Form::open([
                  'route' => ['dishes.destroy', $dish->id],
                  'method' =>'delete',
                  'class'=> 'btn-group'
                  ]) !!}
                  {!!Form::submit('Delete',['class' => 'btn btn-danger'])!!}

                  {!! Form::close() !!}
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
