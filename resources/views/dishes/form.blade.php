@extends('layouts.app')

@section('content')
  <div class="container">
    @if (isset($dish))
      <h1>Dish edit</h1>
    @else
      <h1>Dish create</h1>
    @endif
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          @if (isset($dish))
            <div class="col-md-4">
              <img style="margin: 0 auto;" class="img-responsive" src="{{$dish->photo}}?text={{$dish->title}}" alt="{{$dish->title}}">
            </div>
          @endif
          <div class="col-md-8">
            @if (isset($dish))
              {!! Form::model($dish, ['route' => ['dishes.update', $dish->id],'method' => 'put']) !!}
            @else
              {!! Form::open([
                'route' => 'dishes.store',
                'method' =>'post'
                ]) !!}
              @endif
              <div class="form-group">
                <label for="title">Dish title:</label>
                {!!Form::text('title',null,['class'=>"form-control",'placeholder'=>'Title'])!!}
              </div>

              <div class="form-group">
                <label for="title">Dish description:</label>
                {!!Form::textarea('description',null,['class'=>"form-control",'placeholder'=>'Title'])!!}
              </div>

              <div class="form-group">
                <label for="price">Dish price:</label>
                {!!Form::text('price',null,['class'=>"form-control",])!!}
                <label for="quantity">Dish quantity:</label>
                {!!Form::number('quantity',null,['class'=>"form-control",])!!}
              </div>
              <div class="btn-group">
                @if (isset($dish))
                  <a class="btn btn-default" href="{{ route('dishes.index') }}">Back to dishes</a>
                  {!!Form::submit('Save',['class' => 'btn btn-primary'])!!}
                  {!! Form::close() !!}
                @else
                  {!!Form::submit('Submit',['class' => 'btn btn-primary'])!!}
                  {!! Form::close() !!}
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endsection
