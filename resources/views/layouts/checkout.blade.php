@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <h1>CHECKOUT</h1>

        @if (session('cart.items') && count(session('cart.items'))>0)
        <table class="table table-bordered table-responsive table-condensed checkout-table">
          <thead>
            <tr>
              <th>
                Delete
              </th>
              <th>
                Photo
              </th>
              <th>
                Title
              </th>
              <th>
                Price
              </th>
              <th>
                Netto price
              </th>
              <th>
                Quantity
              </th>
              <th>
                Netto total
              </th>
              <th>
                Total
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach (session('cart.items') as $item)
            <tr>
              <td>
                <a  href="{{route('cart.deleteRow', ['id' =>$item['id']])}}" class="btn btn-danger btn-sm deleteRow" data-id="{{$item['id']}}" href="#">X</a>
              </td>
              <td>
                <img class="img-responsive" src="{{$item['photo']}}" alt="{$item['title']}}">
              </td>
              <td>
                {{$item['title']}}
              </td>
              <td>
                {{ number_format($item['price'], 2)}} &euro;
              </td>
              <td>
                {{ number_format($item['netto_price'],2)}} &euro;
              </td>
              <td>
                {{$item['quantity']}}
              </td>
              <td>
                {{number_format($item['total']/1.21,2)}} &euro;
              </td>
              <td>
                {{number_format($item['total'],2)}} &euro;
              </td>
            </tr>
            @endforeach
          </tr>
          <td colspan="6">
          </td>
          <td>
            Netto total:
          </td>
          <td>
            {{ number_format(session('cart.totalNetto'),2) }} &euro;
          </td>
        </tr>
          </tr>
          <td colspan="6">
          </td>

            <td>
              VAT 21%: 
            </td>
            <td>
              {{ number_format(session('cart.total')-session('cart.totalNetto'),2) }} &euro;
            </td>
          </tr>
        </tr>
          </tr>
          <td colspan="6">
          </td>
            <td>
              Total:
            </td>
            <td>
              {{ number_format(session('cart.total'),2) }} &euro;
            </td>
          </tr>
          </tbody>
        </table>
        <div class="btn-group pull-right">
          <a class="btn btn-danger clear-cart" href="#" >Clear cart</a>
          <a href="{{ route('cart.reservationinfo') }}" class="btn btn-success" href="#">Next</a>
        </div>
        @endif
      </div>
      </div>

  </div>
@endsection
