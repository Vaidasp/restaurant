@extends('layouts.app')

@section('content')
  <div class="container">
    @if (isset($order))
      <h1>Order edit</h1>
    @else
      <h1>Reservation info</h1>
    @endif
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-8">
            @if (isset($order))
              {!! Form::model($order, ['route' => ['orders.update', $order->id],'method' => 'put']) !!}
            @else
              {!! Form::open([
                'route' => 'cart.reservationconfirmation',
                'method' =>'post'
                ]) !!}
              @endif
              <div class="form-group">
                <label for="client_name">Client name:</label>
                {!!Form::text('client_name', Auth::user() ? Auth::user()->name : null, ['class'=>"form-control",'placeholder'=>'Name'])!!}
              </div>

              <div class="form-group">
                <label for="client_email">Client email:</label>
                {!!Form::email('client_email',Auth::user() ? Auth::user()->email : null,['class'=>"form-control",'placeholder'=>'Email'])!!}
              </div>
              <div class="form-group">
                <label for="client_phone">Client phone:</label>
                {!!Form::text('client_phone',Auth::user() ? Auth::user()->phone : null,['class'=>"form-control",'placeholder'=>'Email'])!!}
              </div>
              <div class="form-group">
                <label for="persons">How many persons?:</label>
                {!!Form::selectRange('persons', 1, 8);!!}
              </div>
              <div class="form-group">
                <label for="table">Please select table:</label>
                {!!Form::select('table',$tables,null,['class'=>"form-control"])!!}
              </div>
              <div class="form-group">
                <label for="date">Please select date:</label>
                {!!Form::date('date',null,['class'=>"form-control"])!!}
              </div>
              <div class="form-group">
                <label for="time">Please select time:</label>
                {!!Form::time('time',null,['class'=>"form-control"])!!}
              </div>

              @if (session('cart.total'))
                <p>Total: {{session('cart.total')}} &euro;</p>
              @endif
              <div class="btn-group">
                @if (isset($order))
                  <a class="btn btn-default" href="{{ route('orders.index') }}">Back to orders</a>
                  {!!Form::submit('Save',['class' => 'btn btn-primary'])!!}
                  {!! Form::close() !!}
                @else
                  {!!Form::submit('Confirm reservation',['class' => 'btn btn-primary'])!!}
                  {!! Form::close() !!}
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endsection
