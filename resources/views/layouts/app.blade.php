@php
use App\Contact;
$contact = App\Contact::find(1)->get();
@endphp
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <!-- Scripts -->
  <script>
  window.Laravel = {!! json_encode([
    'csrfToken' => csrf_token(),
    'cartAddRoute'=> route('cart.add'),
    'cartClearRoute'=> route('cart.clear')
    ]) !!};
    </script>
    <script src="//cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'#hours' });</script>
  </head>
  <body>
    <div id="app">
      <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
          <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
              <span class="sr-only">Toggle Navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
              {{ isset($contact) ? $contact[0]->title : config('app.name', 'Laravel') }}
            </a>
          </div>

          <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            @if (Auth::check())
              <ul id="cart-menu" class="nav navbar-nav">
                &nbsp;
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart <span class="caret"></span>
                  </a>

                  <ul id="cart-items" class="dropdown-menu" role="menu">
                    @if (session('cart.items') && count(session('cart.items'))>0)
                      @foreach (session('cart.items') as $item)
                        <li>
                          <a href="#">{{$item['title']}} x {{$item['quantity']}} = {{ $item['total'] }} &euro;</a>
                        </li>
                      @endforeach
                    @endif
                  </ul>
                </li>

                <li><a href="#">Total: <span id="grand_total">{{session('cart.total') ?: '0'}}</span> EUR</a></li>
                <li><a class="clear-cart"  href="#" ><i class="fa fa-trash" aria-hidden="true"></i> Clear cart</a></li>
                <li><a   href="{{ route('cart.checkout') }}" ><i class="fa fa-credit-card" aria-hidden="true"></i> Checkout</a></li>

              </ul>
            @endif

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
              <!-- Authentication Links -->
              @if (Auth::guest())
                <li><a href="{{ route('login') }}">Login</a></li>
                <li><a href="{{ route('register') }}">Register</a></li>
              @else
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <i class="fa fa-user-o" aria-hidden="true"></i> {{ Auth::user()->name }} <span class="caret"></span>
                  </a>

                  <ul class="dropdown-menu" role="menu">
                    @if (Auth::check() && Auth::user()->isAdmin())

                      <li><a class="" href="{{ route('profile') }}">
                        <p>Profile</p>
                      </a></li>
                      <li><a class="" href="{{ route('users.index') }}">
                        <p>Users list</p>
                      </a></li>
                      <li><a class="" href="{{ route('dishes.index') }}">
                        <p>Dishes</p>
                      </a></li>
                      <li><a class="" href="{{ route('orders.index') }}">
                        <p>Orders</p>
                      </a></li>
                      <li><a class="" href="{{ route('tables.index') }}">
                        <p>Tables</p>
                      </a></li>
                      <li>
                        <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        Logout
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                      </form>
                    </li>
                    @else
                      <li><a class="" href="{{ route('profile') }}">
                        <p>Profile</p>
                      </a></li>
                      <li><a class="" href="{{ route('dishes.index') }}">
                        <p>Dishes</p>
                      </a></li>
                      <li><a class="" href="{{ route('orders.index') }}">
                        <p>Orders</p>
                      </a></li>
                      <li>
                        <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        Logout
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                      </form>
                    </li>
                    @endif
                </ul>
              </li>
            @endif
          </ul>
        </div>
      </div>
    </nav>
    @if (session('message'))
      <div class="alert alert-{{session('message')['type']}}">
        {{session('message')['text']}}
      </div>
    @endif
    @yield('content')
    <footer class="footer navbar-fixed-bottom">

      <p><a class="" href="{{ route('contacts.index')}}">
        <i class="fa fa-envelope-o" aria-hidden="true"></i> Contacts
      </a></p>

    </footer>
  </div>

  <!-- Scripts -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/js/bootstrap-formhelpers.min.js" integrity="sha256-H7Mu9l17V/M6Q1gDKdv27je+tbS2QnKmoNcFypq/NIQ=" crossorigin="anonymous"></script>
  <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
