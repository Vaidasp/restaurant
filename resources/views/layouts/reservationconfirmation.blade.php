@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-6">
            <h1>Client: {{session('reservation')['client_name']}}</h1>
            <h3>Email: {{session('reservation')['client_email']}}</h3>
            <h3>Phone: {{session('reservation')['client_phone']}}</h3>
            <h3>Table: {{session('reservation')['table']}}</h3>
            <h3>Date: {{session('reservation')['date']}} {{session('reservation')['time']}}</h3>
          </div>
          <div class="col-md-6">
            <h2>Ordered dishes:</h2>
            <ul class="list-unstyled">
              @foreach (session('cart.items') as $item)
                <h3><li>{{$item['title']}} - {{$item['quantity']}} pcs. x {{$item['price']}} &euro; =  {{$item['total']}} &euro;</li></h3>
              @endforeach
            </ul>
            <h2>Order total: {{session('cart.total')}} &euro;</h2>
          </div>
        </div>
        {!! Form::open([
          'route' => 'orders.store',
          'method' =>'post'
          ]) !!}
          {!!Form::submit('Place order',['class' => 'btn btn-primary'])!!}
          {!! Form::close() !!}
      </div>
    </div>
  </div>
@endsection
