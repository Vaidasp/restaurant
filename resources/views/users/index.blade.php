@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <h1>Users list</h1>
        <table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>
				Actions
			</th>
			<th>
				Name
			</th>
			<th>
				Surname
			</th>
      <th>
        Phone
      </th>
			<th>
				Email
			</th>
      <th>
        Date of birth
      </th>
			<th>
				Type
			</th>
		</tr>
	</thead>
	<tbody>
    @foreach ($users as $user)
      <tr>
        <td class="">
            @if (Auth::check() && Auth::user()->isAdmin())

              <div class="btn-group">
                <a class="btn btn-sm btn-default" href="{{ route('users.show', ['id' => $user->id]) }}">Show</a>
                {!! Form::open([
                  'route' => ['users.destroy', $user->id],
                  'method' =>'delete',
                  'id'=> 'order-id'.$user->id,
                  'class'=>'btn-group'
                  ]) !!}
                  {!!Form::submit('X',['class' => 'btn btn-sm btn-danger'])!!}

                  {!! Form::close() !!}
                </div>
            @else
              <div class="btn-group">
                <a class="btn btn-sm btn-default" href="{{ route('orders.show', ['id' => $order->id]) }}">Show</a>
                </div>
            @endif

        </td>
        <td>
          {{ $user->name }}
        </td>
        <td>
          {{ $user->surname }}
        </td>
        <td>
          {{ $user->phone }}
        </td>
        <td>
          {{ $user->email }}
        </td>
        <td>
          {{ $user->birthdate }}
        </td>
        <td>
          <span class="label label-pill label-primary">{{ ucfirst($user->type) }}</span>
        </td>
      </tr>
    @endforeach
	</tbody>
</table>
      </div>
    </div>
  </div>
@endsection
