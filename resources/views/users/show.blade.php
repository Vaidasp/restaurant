@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-6">
            <h3>User: {{$user->name}} {{ $user->surname }}</h3>
            <h3>Email: <a href= "mailto:{{$user->email}}">{{$user->email}}</a></h3>
            <h3>Phone: {{$user->phone}}</h3>
          </div>
          <div class="col-md-6">
            <h3>Country: {{$user->country}}</h3>
            <h3>City: {{$user->city}}</h3>
            <h3>Address: {{$user->address}}</h3>
            <h3>Zip code: {{$user->zipcode}}</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <h2>Orders</h2>
            <div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">All orders</a></li>

  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="allorders">
      <table class="table table-bordered">
<thead>
  <tr>
    <th>
      Actions
    </th>
    <th>
      Name
    </th>
    <th>
      Email
    </th>
    <th>
      Total
    </th>
    <th>
      Reservation date
    </th>
  </tr>
</thead>
<tbody>
  @foreach ($user->orders as $order)
    <tr class="{{$order->getOrderStatus()}}">
      <td class="table-edit">
          @if (Auth::check() && Auth::user()->isAdmin())

            <div class="btn-group">
              <a class="btn btn-sm btn-default" href="{{ route('orders.show', ['id' => $order->id]) }}">Show</a>
              <a class="btn btn-sm btn-success" href="{{ route('orders.edit', ['id' => $order->id]) }}">Edit</a>
              {!! Form::open([
                'route' => ['orders.destroy', $order->id],
                'method' =>'delete',
                'id'=> 'order-id'.$order->id,
                'class'=>'btn-group'
                ]) !!}
                {!!Form::submit('X',['class' => 'btn btn-sm btn-danger'])!!}

                {!! Form::close() !!}
              </div>
          @else
            <div class="btn-group">
              <a class="btn btn-sm btn-default" href="{{ route('orders.show', ['id' => $order->id]) }}">Show</a>
              </div>
          @endif

      </td>
      <td>
        {{ $order->client_name }}
      </td>
      <td>
        {{ $order->client_email }}
      </td>
      <td>
        {{ $order->total }}
      </td>
      <td>
        {{ $order->reservation_date }}
      </td>
    </tr>
  @endforeach
</tbody>
</table>
  </div>

  </div>

</div>
          </div>
        </div>
        {{-- <h1>Order number: {{$order->id}} </h1>
        <h2>Client: {{$order->client_name}} </h2>
        <h2>Table: {{$order->table->title}} </h2>
        <h2>Number of persons: {{$order->number_of_persons}} </h2>
        <h2>Reservation time: {{$order->reservation_date}}  {{$order->reservation_time}}</h2>
        <h1>Ordered dishes:</h1>
            <table class="table table-bordered table-responsive checkout-table">
              <thead>
                <tr>
                  <th>
                    Photo
                  </th>
                  <th>
                    Title
                  </th>
                  <th>
                    Price
                  </th>
                  <th>
                    Quantity
                  </th>
                  <th>
                    Total
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($order->order_lines as $item)
                <tr>
                  <td>
                    <img class="img-responsive" src="{{$item->dish->photo}}" alt="{{$item->dish->title}}">
                  </td>
                  <td>
                    {{$item->dish->title}}
                  </td>
                  <td>
                    {{$item->dish->price}} &euro;
                  </td>
                  <td>
                    {{$item->quantity}}
                  </td>
                  <td>
                    {{$item['total']}} &euro;
                  </td>
                </tr>
                @endforeach
              </tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                  Total:
                </td>
                <td>
                  {{$order->total}} &euro;
                </td>
              </tr>
              </tbody>
            </table> --}}
      </div>
    </div>
  </div>
@endsection
