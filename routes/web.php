<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DishController@index');

// Profile routes
Route::get('/profile', 'UserController@profile')->name('profile')->middleware('auth');
Route::put('/profile', 'UserController@update')->name('profile.update')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('contacts', 'ContactController');

Route::resource('dishes', 'DishController');

Route::resource('orders', 'OrderController');


Route::delete('orders/delete-line/{lineId}', 'OrderController@destroyLine')->name('orders.destroyLine');

Route::post('orders/add-line/{orderId}', 'OrderController@addLine')->name('orders.addLine');


Route::resource('tables', 'TableController');

Route::resource('users', 'UserController');

Route::get('/profile', 'UserController@profile')->name('profile');

Route::post('cart', 'OrderController@addToCart')->name('cart.add');

Route::delete('cart', 'OrderController@ClearCart')->name('cart.clear');

Route::get('/checkout', 'OrderController@CartCheckout')->name('cart.checkout')->middleware('auth');

Route::get('cart/delete/{id}', 'OrderController@cartDeleteRow')->name('cart.deleteRow');

Route::get('/reservationinfo', 'CheckoutController@ReservationInfo')->name('cart.reservationinfo');

Route::post('/reservationconfirmation', 'CheckoutController@ReservationConfirmation')->name('cart.reservationconfirmation');
