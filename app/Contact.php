<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
  protected $fillable = [
      'title', 'address', 'email', 'phone', 'hours', 'map'
  ];

  public function getMapSrc() {
      $map = explode('"', $this->map);
      return $map[1];
  }
}
