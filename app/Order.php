<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $fillable = [
    'name', 'email', 'total', 'due_date', 'user_id'
  ];

  public function order_lines() {
    return $this->hasMany('\App\OrderLine');
  }

  public function user() {
    return $this->belongsTo('\App\User');
  }

  public function table() {
    return $this->belongsTo('\App\Table');
  }

  public function getOrderStatus(){
    $reservation_date = \Carbon\Carbon::parse($this->reservation_date);

    if ($reservation_date->isToday()) {
      return "danger";
    }  elseif ($reservation_date->isTomorrow()) {
      return "warning";
    } elseif ($reservation_date->isPast()) {
      return "success";
    }
      return "primary";
  }


}
