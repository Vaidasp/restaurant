<?php

namespace App\Http\Controllers;
use App\Table;
use App\Order;
use App\OrderLine;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{

  public function __construct() {

          $this->middleware('auth');
      }

  public function ReservationInfo(){

    $tables = Table::all()->pluck('title','id');

    return view('layouts.checkoutform', compact('tables'));
  }

  public function ReservationConfirmation(Request $request){
      $reservation = [
      'client_name'=>$request->client_name,
      'client_email'=>$request->client_email,
      'client_phone'=>$request->client_phone,
      'persons'=>$request->persons,
      'table'=>$request->table,
      'date'=>$request->date,
      'time'=>$request->time,
      ];

      $this->validate($request, [

          'client_name' => 'required|max:255',
          'client_email' => 'required|email|max:255',
          'client_phone' => 'required|numeric',
          'persons' => 'required|numeric|between:0,15',
          'table' => 'required|exists:tables,id',
          'date' => 'required|date|date_format:Y-m-d',
          'time' => 'required',

       ]);


      session(['reservation' => $reservation ]);

    return view('layouts.reservationconfirmation');
  }



}
