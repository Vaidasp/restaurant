<?php

namespace App\Http\Controllers;

use App\Dish;
use Illuminate\Http\Request;

class DishController extends Controller
{

  // public function __construct() {
  //   $this->middleware('auth',
  //   ['except'=>['index','show']]
  // );
  // }

  public function __construct() {
          $this->middleware('authAdmin')
               ->except(['index', 'show']);
      }


  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $dishes = Dish::all();
    return view('dishes.index', compact('dishes'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return view('dishes.form');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */


  public function store(Request $request)
  {
      $dish = new Dish();
      $dish->title = $request->title;
      $dish->description = $request->description;
      $dish->price = $request->price;
      $dish->netto_price = $request->price/1.21;
      $dish->photo='http://placehold.it/300';
      $dish->quantity = $request->quantity;

      $dish->save();
      return redirect()->route('dishes.index');
  }

  /**
  * Display the specified resource.
  *
  * @param  \App\Dish  $dish
  * @return \Illuminate\Http\Response
  */
  public function show(Dish $dish)
  {
    return view('dishes.show', compact('dish'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Dish  $dish
  * @return \Illuminate\Http\Response
  */
  public function edit(Dish $dish)
  {
    return view('dishes.form', compact('dish'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Dish  $dish
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, Dish $dish)
  {
    $dish->title = $request->title;
    $dish->description = $request->description;
    $dish->price = $request->price;
    $dish->netto_price = $request->price/1.21;
    $dish->quantity = $request->quantity;

    $dish->update();
    return redirect()->route('dishes.index');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Dish  $dish
  * @return \Illuminate\Http\Response
  */
  public function destroy(Dish $dish)
  {
    $dish->delete();
    return redirect()->route('dishes.index');
  }
}
