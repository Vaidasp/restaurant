<?php

namespace App\Http\Controllers;

use App\Order;
use App\Table;
use App\OrderLine;
use App\Dish;
use Illuminate\Http\Request;

class OrderController extends Controller
{

  public function __construct() {
    $this->middleware('authAdmin')
    ->except(['index', 'show', 'store','addToCart','ClearCart', 'cartDeleteRow', 'CartCheckout']);

    $this->middleware('auth')->except(['index', 'show']);
  }


  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    if (\Auth::user()->type == ('admin')) {
      $orders = Order::orderBy('reservation_date', 'desc')->get();
      // $orders = Order::orderBy('reservation_date', 'desc')->all();
    } else {
      $orders = \Auth::user()->orders;
    }

    return view('orders.index', compact('orders'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $tables = Table::all()->pluck('title','id');

    if (session('cart.total')) {
      return view('orders.form', compact('tables'));

    } else {
      return redirect()->route('dishes.index');
    }

  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    // dd($request);
    $order = new Order();
    $order->client_name = session('reservation')['client_name'];
    $order->client_email = session('reservation')['client_email'];
    $order->due_date = \Carbon\Carbon::now();
    $order->total = session('cart.total');
    $order->user_id = \Auth::user()->id;
    $order->table_id = session('reservation')['table'];
    $order->reservation_date = session('reservation')['date'];
    $order->reservation_time = session('reservation')['time'];
    $order->number_of_persons = session('reservation')['persons'];
    $order->save();

    foreach (session('cart.items') as $item) {
      OrderLine::create([
        'order_id'=>$order->id,
        'dish_id'=>$item['id'],
        'quantity'=>$item['quantity'],
        'total'=>$item['total']
      ]);
    }
    $this->ClearCart();

    return redirect()->route('contacts.index')->with('message',[
      'text'=>'Uzsakymas sekmingai priimtas',
      'type'=>'success'
    ]);
  }

  /**
  * Display the specified resource.
  *
  * @param  \App\Order  $order
  * @return \Illuminate\Http\Response
  */
  public function show(Order $order)
  {
    return view('orders.show', compact('order'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Order  $order
  * @return \Illuminate\Http\Response
  */
  public function edit(Order $order)
  {
    $tables = Table::all()->pluck('title','id');
    $dishes = Dish::all()->pluck('title','id');
    return view('orders.form', compact('order','tables','dishes'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Order  $order
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, Order $order)
  {
    $this->validate($request, [

      'client_name' => 'required|max:255',
      'client_email' => 'required|email|max:255',
      'client_phone' => 'required|numeric',
      'persons' => 'required|numeric|between:0,15',
      'table' => 'required|exists:tables,id',
      'reservation_date' => 'required|date|date_format:Y-m-d',
      'reservation_time' => 'required'

    ]);

    $order->client_name = $request->client_name;
    $order->client_email = $request->client_email;
    $order->reservation_date = $request->reservation_date;
    $order->reservation_time = $request->reservation_time;
    $order->table_id = $request->table;
    $order->number_of_persons = $request->persons;
    $order->due_date = '2017-03-18';

    $order->update();
    return redirect()->route('orders.index');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Order  $order
  * @return \Illuminate\Http\Response
  */
  public function destroy(Order $order)
  {
    foreach ($order->order_lines as $line ) {
      $line->delete();
    }

    $order->delete();
    return redirect()->route('orders.index');
  }

  // Destroy dish from order edit form

  public function destroyLine($lineId)
  {
    $orderline = Orderline::find($lineId);
    $order=$orderline->order;
    $order->total-=$orderline->total;
    $order->save();

    $orderline->order->total-=$orderline->total;
    $orderline->delete();

    return redirect()->route('orders.edit',compact('order'))->with('message',[
      'text'=>''.$orderline->quantity . ' pcs. of '. $orderline->dish->title . ' removed!',
      'type'=>'success'
    ]);;
  }



  public function addLine(Request $request,$orderId)
  {
    $order = Order::find($orderId);
    $dish=Dish::find($request->dish_id);
    $exists=false;
    // dd($order->order_lines[0]);
    foreach ($order->order_lines as $line) {
      if ($line['dish_id'] == $request->dish_id) {
        $line['quantity'] += $request->dish_quantity;
        $line['total'] += $request->dish_quantity * $line->dish->price ;
        $line->save();
        $order->total += $request->dish_quantity * $line->dish->price;
        $order->save();
        $exists=true;
        break;
      }
    }
      if (!$exists) {
        OrderLine::create([
          'order_id'=>$order->id,
          'dish_id'=>$request->dish_id,
          'quantity'=>$request->dish_quantity,
          'total'=>$request->dish_quantity * $dish->price
        ]);
        $order->total += $request->dish_quantity * $dish->price;
        $order->save();
      }

    return redirect()->route('orders.edit',compact('order'))->with('message',[
      'text'=>''.$request->dish_quantity . ' pcs. of '. $dish->title . ' added!',
      'type'=>'success'
    ]);
  }



  public function addToCart(Request $request)
  {
    // session()->flush();
    // return;
    $dish_id = $request->id;
    $dish = Dish::find($dish_id);
    $product= [
      'id'=> $dish->id,
      'title'=> $dish->title,
      'photo'=>$dish->photo,
      'price'=>$dish->price,
      'netto_price'=>$dish->getNetto(),
      'quantity' => 1,
      'total'=> $dish->price,
    ];
    $items=session('cart.items');
    $existing=false;
    $grand_total = 0;

    if ($items && count($items) > 0) {
      foreach ($items as $index => $item) {
        if ($item['id']== $dish_id) {
          $items[$index]['quantity'] = $item['quantity']+$product['quantity'];
          $items[$index]['total'] = $items[$index]['quantity']*$dish->price;
          $existing=true;
          $grand_total+=$items[$index]['total'];
        } else {
          $grand_total += $item['total'];
        }
      }
    }

    if (!$existing) {
      session()->push('cart.items', $product);
      $grand_total+=$product['total'];
    } else {
      session(['cart.items'=>$items]);
    }
    session(['cart.total'=>$grand_total,
    'cart.totalNetto'=>$grand_total/1.21,
    'cartVAT'=>$grand_total - ($grand_total/1.21)
  ]);
  return session('cart');
}

public function ClearCart(){
  session(['cart.items'=>[], 'cart.total'=> 0]);
}

public function CartCheckout(){
  return view('layouts.checkout');
}

public function cartDeleteRow(Request $request){
  $id=$request->id;
  $items = session('cart.items');
  $total = session('cart.total');

  foreach ($items as $index => $item) {
    if ($item['id'] == $id) {
      $total -= $item['total'];

      unset($items[$index]);
      break;
    }
  }

  session([
    'cart.items'=>$items,
    'cart.total'=>$total,
    'cart.totalNetto'=>$total/1.21
  ]);

  return redirect()->route('cart.checkout');
}

}
