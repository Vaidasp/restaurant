<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct() {
             $this->middleware('authAdmin')
                  ->except(['profile', 'update']);

             $this->middleware('auth');
         }

         public function index()
         {
           $users = User::all();
           return view('users.index', compact('users'));
         }

      public function profile()
      {
          $user = \Auth::user();
          $title = 'Update profile';
          return view('auth.register', compact('user', 'title'));
      }

      public function update(Request $request, User $user)
      {
          $user = \Auth::user();

          $user->name = $request->name;
          $user->surname = $request->surname;
          $user->birthdate = $request->birthdate;
          $user->phone = $request->phone;
          $user->city = $request->city;
          $user->address = $request->address;
          $user->country = $request->country;
          $user->zipcode = $request->zipcode;
          $user->save();

          return redirect()->route('dishes.index')->with('message', [
              'text' => 'Profile updated!',
              'type' => 'success'
          ]);
      }

      public function show(User $user)
      {
        return view('users.show', compact('user'));
      }

      public function edit(User $user)
      {
        // $title = 'Update profile';
        // return view('auth.register', compact('user', 'title'));
      }

      public function destroy(User $user)
      {
        foreach ($user->orders as $order) {
        $order->delete();
        }
        $user->delete();
        return redirect()->route('users.index');
      }

}
