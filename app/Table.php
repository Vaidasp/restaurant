<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
  protected $fillable = [
      'title', 'photo', 'min', 'max'
  ];

  public function orders() {
    return $this->hasMany('\App\Order');
  }
}
