<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
//     public function getFormatedPriceAttribute(){
//       return number_format($this->price/100,2);
// }
//
// public function getNettoPriceAttribute($value){
//   return number_format($value/100,2);
// }

public function order_lines() {
  return $this->hasMany('\App\OrderLine');
}

public function getNetto(){
return $this->price/1.21;
}
}
