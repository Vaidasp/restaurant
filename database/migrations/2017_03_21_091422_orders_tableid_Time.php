<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdersTableidTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('orders', function (Blueprint $table) {
          $table->integer('table_id')->unsigned()->nullable();
          $table->date('reservation_date')->nullable();
          $table->time('reservation_time')->nullable();
          $table->integer('number_of_persons')->unsigned();
          $table->foreign('table_id')->references('id')->on('tables');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
